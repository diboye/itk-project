#include <TStyle.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>
#include <TNtuple.h>
#include <TSystem.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TFitResult.h>
#include <TObject.h>
#include <TTree.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <TBox.h>
#include <TSpectrum.h>
#include <TLatex.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string.h>
#include <TGraph2DErrors.h>
#include <cmath>
#include <iomanip>
#include "RooWorkspace.h"
#include <RooRealVar.h>
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooAbsArg.h"
#include "RooMomentMorph.h"
#include "RooFit.h"
#include "TRandom3.h"
#include "TGraphErrors.h"
#include "TGaxis.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TSpline.h"
#include "TPaveStats.h"
#include "TExec.h"
#include "TPaletteAxis.h"
#include <TString.h>
#include "simulation.h"
using namespace std;

bool add_value( std::map< std::string, std::set<string> >& map,
                const std::string& key, string value )
{
    return map[key].insert(value).second ;
}


int main()
{
  TString newfile;
  TString myfile[]={"input/Meas.20201216131337.txt"};
  ifstream fin(myfile[0]);
  string line;

 std::map< std::string, std::set<string> > map ;

    const auto try_add = [&] ( const std::string& key, string value )
    {
     
      std::cout << "key '" << key << "'  value " << value << " : " ;
        if( add_value( map, key, value ) ) std::cout << "inserted value\n" ;
        else std::cout << "value is already present for this key\n" ;
	
    };

    try_add(s_temp_rh[1], s_temp_rh[6]); 
    try_add(s_temp_rh[2], s_temp_rh[7]); 
    try_add(s_temp_rh[3], s_temp_rh[8]); 
    try_add(s_temp_rh[4], s_temp_rh[9]);
    try_add(s_temp_rh[5], s_temp_rh[10]); 
    try_add(s_temp_rh[11], s_temp_rh[23]);
    try_add(s_temp_rh[13], s_temp_rh[24]);
    try_add(s_temp_rh[15], s_temp_rh[25]);
    try_add(s_temp_rh[12], s_temp_rh[26]);
    try_add(s_temp_rh[14], s_temp_rh[27]);
    try_add(s_temp_rh[16], s_temp_rh[28]);
    try_add(s_temp_rh[11], s_temp_rh[29]);
    try_add(s_temp_rh[13], s_temp_rh[30]);
    try_add(s_temp_rh[15], s_temp_rh[31]);
    try_add(s_temp_rh[12], s_temp_rh[32]);
    try_add(s_temp_rh[14], s_temp_rh[33]);
    try_add(s_temp_rh[16], s_temp_rh[34]);
    try_add(s_temp_rh[11], s_temp_rh[35]);
    try_add(s_temp_rh[13], s_temp_rh[36]);
    try_add(s_temp_rh[15], s_temp_rh[37]);
    try_add(s_temp_rh[12], s_temp_rh[38]);
    try_add(s_temp_rh[14], s_temp_rh[39]);
    try_add(s_temp_rh[16], s_temp_rh[40]);
    try_add(s_temp_rh[17], s_temp_rh[41]);
    try_add(s_temp_rh[19], s_temp_rh[42]);
    try_add(s_temp_rh[21], s_temp_rh[43]);
    try_add(s_temp_rh[18], s_temp_rh[44]);
    try_add(s_temp_rh[20], s_temp_rh[45]);
    try_add(s_temp_rh[22], s_temp_rh[46]);
    try_add(s_temp_rh[17], s_temp_rh[47]);
    try_add(s_temp_rh[19], s_temp_rh[48]);
    try_add(s_temp_rh[21], s_temp_rh[49]);
    try_add(s_temp_rh[18], s_temp_rh[50]);
    try_add(s_temp_rh[20], s_temp_rh[51]);
    try_add(s_temp_rh[22], s_temp_rh[52]);
    try_add(s_temp_rh[17], s_temp_rh[53]);
    try_add(s_temp_rh[19], s_temp_rh[54]);
    try_add(s_temp_rh[21], s_temp_rh[55]);
    try_add(s_temp_rh[18], s_temp_rh[56]);
    try_add(s_temp_rh[20], s_temp_rh[57]);
    try_add(s_temp_rh[22], s_temp_rh[58]);

    std::cout << "\nthe map contains:\n--------------\n" ;
    for( const auto& [key,set] : map )
    {
        std::cout << "Temp fiber '" << key << "'  <==> RH fiber: [" ;
        for( string value : set ) std::cout << value << "  " ;
        std::cout << "]\n" ;
    }
  
  vector<string> lineContainer;
  
  if (fin.is_open())
    {
      getline(fin, line);
      while ( getline (fin,line) )
	{
	  lineContainer.push_back(line);
	}
      fin.close();
    }
  // cout << " line : " << lineContainer[0] << '\n';
  //cout << " line : " << s_temp_rh[0] << '\n';
  int NbMeas = lineContainer.size();
  int n = lineContainer[1].length();
  char str[n + 1];
  vector<char*> wordContainer;
  vector<Double_t> numbContainer[NbMeas];
  //cout << " n : " << n << '\n';
  // declaring character array
 

  for (int k=0; k<NbMeas; k++)
    {
  
  // copying the contents of the
  // string to char array
  strcpy(str, lineContainer[k].c_str());
  //strcpy(Numb, lineCointainer[1].c_str());


  char delim[] = " ";
  
  char *ptr = strtok(str, delim);
  
  while(ptr != NULL)
    {
      //printf("'%s'\n", ptr);
      wordContainer.push_back(ptr);
      ptr = strtok(NULL, delim);
    }
  
  
  
  //cout << " test " << atof(wordContainer[k]) << endl;
  for (int i =0; i < wordContainer.size(); i++)
      {
	numbContainer[k].push_back(atof(wordContainer[i]));
      }
  
  //numbCointainer= atof(wordCointainer);
  wordContainer.clear();
  memset(str, 0, sizeof str);
    }
  
  //cout << " size " << numbContainer[0].size() << endl;
  //cout << " check " << numbContainer[0][2] << endl; //first [] corresponds to Line, second [] corresponds to Colone of Meas.20201216131337.txt file
  //cout << " check " << numbContainer[0][6] << endl;
 
  
   vector<double> RH[41];
   vector<double> TEMP[41];
   vector<double> DP[41];

   for (int l=0; l<41; l++)
     {

  for (int i =0; i < NbMeas; i++)
      {
	RH[l].push_back(numbContainer[i][rh_column[l]]);
	TEMP[l].push_back(numbContainer[i][temp_column[l]]);
	DP[l].push_back(myFunc(numbContainer[i][temp_column[l]], numbContainer[i][rh_column[l]]));

      }
     }
   std::cout << "\n" ;
   cout << "******************** please choose from 0 to 40 the desired DP corresponding to the  following RH fiber **********" <<endl;

   std::cout << "\n" ;
   for (int i=0; i<41; i++)
     {
       cout << i << " " << "rh fiber: " << s_rh[i] << endl;;
     }


   int fiber;
  cout << "Please enter the number: ";
  cin >> fiber;
  cout << "The RH fiber chosen is " << s_rh[fiber] <<"\n";
  //cout << " and its double is " << fiber*2 << ".\n";

  std::ofstream outfile_rh, outfile_temp, outfile_dp;
  outfile_dp.open(TString::Format("%s_dp.txt", s_rh[fiber].c_str()));
  outfile_temp.open(TString::Format("%s_temp.txt", s_rh[fiber].c_str()));
  outfile_rh.open(TString::Format("%s_rh.txt", s_rh[fiber].c_str()));
  
  TCanvas *c1 = new TCanvas("c1","c1",700,300);
  TPad* thePad = (TPad*)c1->cd();
  
  TH1* histDP = new TH1D("histDP", TString::Format("%s_dp", s_rh[fiber].c_str()), 100, -60, -20);
  histDP->GetXaxis()->SetTitle("DP");
  histDP->GetYaxis()->SetTitle("events/[0.4]");

  TH1* histTemp = new TH1D("histTemp", TString::Format("%s_Temp", s_rh[fiber].c_str()), 75, 0, 30);
  histTemp->GetXaxis()->SetTitle("Temp ^{o}C");
  histTemp->GetYaxis()->SetTitle("events/[0.4]");

  TH1* histRH = new TH1D("histRH", TString::Format("%s", s_rh[fiber].c_str()), 80, 0, 2);
  histRH->GetXaxis()->SetTitle("RH");
  histRH->GetYaxis()->SetTitle("events/[0.025]");
  
   for (int k =0; k<NbMeas; k++)
    
    {
      //cout << "RH " << RH[0][k] << endl; //first [] correspond to the column, second [] correponds to line
      //cout << "TEMP " << TEMP[0][k] << endl;
      //cout << "DP " << DP[fiber][k] << endl;
      outfile_dp << DP[fiber][k] << endl;
      outfile_temp << TEMP[fiber][k] << endl;
      outfile_rh << RH[fiber][k] << endl;
      histDP->Fill(DP[fiber][k]);
      histTemp->Fill(TEMP[fiber][k]);
      histRH->Fill(RH[fiber][k]);
    }
  //cout << "temp = " << TEMP[0][0]  << " RH = " << RH[0][0] << " result = " << myFunc(RH[0][0],TEMP[0][0]) << endl;
  //myFunc(TEMP[0][0], RH[0][0]);
   gRandom = new TRandom3();
   c1->Divide(3,1);
   c1->cd(1);
   histTemp->Draw();
   c1->cd(2);
   histRH->Draw();
   c1->cd(3);
   histDP->Draw();
   
c1->Print(TString::Format("%s.pdf", s_rh[fiber].c_str()));
 c1->Clear();
 TH1D * histSim = new TH1D("histSim",TString::Format("%s_Sim", s_rh[fiber].c_str()), 100, -60, -20);
 // fill in the histogram
 for (int l =0; l< 1; l++){ // here we create the stream by varying the mean and sigma of gaussian simulation : for now it's put only 1.
 for (int i = 0; i < histDP->GetMaximum(); ++i) // random number can also be used instead of the maximum of the hist.
   {
     histSim->Fill(gRandom->Gaus(histDP->GetMean(), histDP->GetRMS()));
   }
 histSim->Draw();
 c1->Print(TString::Format("%s_sim.pdf", s_rh[fiber].c_str()));
 }
 
 

  
  return 0;
}
